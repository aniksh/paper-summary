\documentclass[11pt]{article}

\usepackage[margin=1in]{geometry}
\usepackage{amsmath, amsfonts, amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\hypersetup{
    colorlinks   = true, %Colours links instead of ugly boxes
    urlcolor     = red, %Colour for external hyperlinks
    linkcolor    = cyan, %Colour of internal links
    citecolor   = blue %Colour of citations
}
\usepackage{pdflscape}
\usepackage{adjustbox}

\usepackage{natbib}
\bibliographystyle{plainnat}

\begin{document}
\title{Performance Evaluation of Multi-sense Word Embeddings in Downstream 
    Tasks}
\author{Anik Saha}
\maketitle

\section{Introduction}
Words have different meanings when used  in 
different contexts. So representing a word with 
multiple embeddings should result in more robust 
and interpretable word vector models. There has been 
a number of multi-sense word embedding models in 
recent years that share this property of learning 
multiple vector representation for each word. Most of these models have been evaluated in word 
similarity tasks, but the value of word embeddings 
is their use in downstream NLP applications. We want 
to know how these models will fare in the real world 
with powerful and state-of-the-art deep neural 
networks in different tasks. \cite{li_multi-sense_2015} showed their multi-sense
embedding model was effective in some cases but 
using a higher dimensional single-sense embedding 
offered better performance. But there has not been a
large scale comparison of multi-sense embeddings 
with their single-sense counterparts in these 
downstream tasks. Our goal is to find out the best 
way to incorporate these multi-sense embeddings in a 
downstream task and determine if there is any 
advantage to these multi-sense models compared to
traditional word2vec models.

\section{Word Embedding Models}
We are evaluating the following multi-sense embedding models: Gaussian mixture 
model (Word2GM \citep{athiwaratkun_multimodal_2017}), Probabilistic FastText 
(PFTGM \citep{athiwaratkun_probabilistic_2018}), Context2vec 
\citep{melamud_context2vec_2016}, Multi-sense embeddings (CRP 
\citep{li_multi-sense_2015}) and Bayesian Skip-gram (BSG \citep{brazinskas_embedding_2018}). \\

We train all of these embedding models on the ukWac dataset containing $\approx 
2$ billion words. All the models are trained for 3 epochs and the dimension is 
set to 100 for a balanced comparison.

\subsection{Gaussian Mixture Model (Word2GM)}
Word embeddings in a vector space are point representations containing semantic 
information. In this embedding model, a word is represented by a multimodal 
distribution, specifically a mixture of Gaussians. The mean of the mixtures 
represent the deterministic point vectors corresponding to different meanings
of a word. 

\begin{figure}[h]
    \centering
    \includegraphics[width=.5\linewidth]{figs/gmm-embed}
    \caption{Top: Gaussian mixture embedding that has different Gaussian 
        component for different meanings of the word 'rock'. Bottom: Gaussian 
        embedding where words with multiple meaning have a high variance to cover  
        the different meanings}
\end{figure}

\subsubsection{Learning Method}
The distribution is learned using the max-margin ranking objective
\[
L_\theta (w,c,c') = \max(0, m - \log E_\theta (w,c) + \log E_\theta (w,c'))
\]
where $c$ is a context word and $c'$ is a negative sample (out of context) word 
for $w$.  The similarity of a word and its positive context are pushed higher 
than that of its negative context by the margin $m$. They use subsampling and 
negative sampling like the origninal $word2vec$ method.

The energy function is 
\[
E (f,g) = \int f(x) g(x) dx = \left< f,g \right >_{L_2}
\]
where $\left< \right >_{L_2}$ is the inner product in the Hilbert space $L_2$. 
This energy function has a closed form for Gaussian mixtures. So it is a 
better choice than KL divergence.

\subsubsection{Experiment}
We selected two mixtures for the model as the original paper suggested that 
using three mixtures does not provide significant difference in performance.
We use two methods to use these Gaussian mixture embeddings in downstream tasks
where each word has a unique point embedding in a vector space. 
\begin{description}
    \item[Average] In the simplest approach, we take the average of the two 
    means of the mixtures.
    \item[Best] Here we select the mixture cluster for the target word based on 
    the context words. We compute pairwise cosine similarity of the target 
    word with all context words and select the mixture with the maximum score.
    For context words $c_1, c_2, \dots , c_J$ and target word $w$ with $K$ 
    mixtures per embedding, the best cluster for $w$ is
    \[
    cl^* = \arg \max_{i=1}^K \{\max_{j=1,k=1}^{J,K} CosSim(w^i, c_j^k)\}
    \]
\end{description}

\subsection{Probabilistic FastText (PFTGM)}
This method extends the Gaussian mixture model of embeddings. They represent 
each word with a Gaussian mixture density, where the mean of a mixture 
component is given by the sum of character n-grams. The concept of summing 
character n-grams to get word representation is based on \textsc{FastText}. 
This allows the model to learn representation for sub-word structures, 
producing accurate representations of rare, misspelt, or unseen words.

\subsubsection{Learning Method}
A word $w$ is associated with a density function
\[
f(x) = \sum_{i=1}^K p_{w,i}\mathcal{N}(x;\mu_{w,i}, \Sigma_{w,i})
\]
where $\mu_{w,i}$ are the mean vectors and $\Sigma_{w,i}$ are the covariance 
matrices and $\sum_{i=1}^K p_{w,i} = 1$. \\

For word $w$, the mean vector $\mu_w$ is estimated with the average over n-gram 
vectors and its dictionary-level vector. 
\[
\mu_w = \frac{1}{|NG_w| + 1} \left( v_w + \sum_{g\in NG_w} z_g \right)
\]
where $z_g$ is the vector character n-gram $g$, $v_w$ is the word vector and 
$NG_w$ is the set of character n-grams for the word $w$. \\

Examples of 3,4-grams for a word "beautiful", including the beginning-of-word 
character '$<$' and end-of-word character '$>$', are:
\begin{itemize}
    \item 3-grams: $<$be, bea, eau, aut, uti, tif, ful, ul$>$
    \item  4-grams: $<$bea, beau .., iful ,ful$>$
\end{itemize}

They use the same loss function as the Gaussian mixture paper. 
\[
L(f, g) = max [0, m - E(f, g) + E(f, n)]
\]

They have a simplified energy function where they assume spherical covariance 
instead of a diagonal covariance and use the scale of covariance matrix as a 
hyperparameter.

\subsubsection{Experiment}
Similar to Word2GM (Gaussian mixture model) we use 2 mixtures for the word 
embeddings. To select one of these components in a downstream task, we use 
the same approach as before.
\begin{description}
    \item[Average] In the simplest approach, we take the average of the two 
    means of the mixtures.
    \item[Best] Here we select the mixture cluster for the target word based on 
    the context words. We compute pairwise cosine similarity of the target 
    word with all context words and select the mixture with the maximum score.
    For context words $c_1, c_2, \dots , c_J$ and target word $w$ with $K$ 
    mixtures per embedding, the best cluster for $w$ is
    \[
    cl^* = \arg \max_{i=1}^K \{\max_{j=1,k=1}^{J,K} CosSim(w^i, c_j^k)\}
    \]
\end{description}

\subsection{Context2vec}
This is a neural model for generating an embedding for a context using a 
bidirectional LSTM. This model reached state-of-the-art results on sentence 
completion, lexical substitution tasks which proves the effectiveness of 
context embedding. We expect that we can combine this context embedding in a
logical way to augment the ability of word embeddings to represent meaning in 
different contexts.

\subsubsection{Model Details}
This model is based on word2vec CBOW \citep{mikolov_efficient_2013} 
architecture where the average of word embeddings for the context is replaced 
by a bidirectional LSTM.

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{figs/c2v-arch}
    \caption{Context2vec architectre}
\end{figure}

Given a sentence $w_{1:n}$, the bidirectionalLSTM context representation for 
the target $w_i$ is the vector concatenation
\[
biLS(w_{1:n},i) = iLSTM(w_{1:i-1}) \oplus rLSTM(w_{n:i+1})
\]
where $l/r$ denotes left-to-right / right-to-left LSTM. Then two layer MLP 
with ReLU activation is applied to this. So the context vector is
\[
\overrightarrow{c} = L_2(ReLU(L_1(biLS(w_{1:n,i}))))
\]
Here, $L_i(x) = W_ix + b_i$. The target word embedding is $\overrightarrow{t}$ 
and the loss function is the word2vec negative sampling objective function.
\[
L(\overrightarrow{t}, \overrightarrow{c}) = \sum_{\overrightarrow{t}, 
\overrightarrow{c}} \left( \log \sigma(\overrightarrow{t} \cdot 
\overrightarrow{c}) + \sum_{i=1}^k \log \sigma( - \overrightarrow{t} \cdot 
\overrightarrow{c}) \right)
\]

With this model, we can use word-to-word, word-to-context and context-to-
context similarity to find similar words given a context.

\subsubsection{Experiment}
\begin{itemize}
    \item We can use the target word embedding ($\overrightarrow{t}$) like a single-sense embedding in downstream applications. 
    \item Second approach is to concatenate the context embedding 
    ($\overrightarrow{c}$) with the target word embedding ($\overrightarrow{t}$)
\end{itemize}

\subsection{Multi-sense Embeddings (CRP)}
This model learns multiple sense representation for each word based on Chinese 
Restaurant Processes and provides a method for evaluating these embeddings in 
different language understanding tasks. Even though several multi-sense 
embedding models were published before, this was the first paper to incorporate 
these embeddings in downstream tasks.

\subsubsection{Model Details}
This model does not have a fixed number of sense embeddings for each word, 
rather they learn the number of senses and corresponding sense embeddings 
jointly from the corpus. Chinese Restaurant Process is used to decide when a 
word in the corpus should be assigned a new sense. This follows the 
probability distribution:
\[
Pr(t_w = t) \propto \begin{cases}
N_t P(w|d_t) & \text{if $t$ already exists} \\
\gamma P(w|d_{new}) & \text{if $t$ is new}
\end{cases}
\]

where $N_t$ denotes the number of words already assigned to sense t and $P(w|d_t)$ denotes the
probability of assigning the current data point to
cluster $d_t$, $\gamma$ is the hyper parameter 
controlling the preference for choosing a new sense.

Each token $w$ has a $K$ dimensional 
global embedding $e_w$ like a standard word embedding
model. It is also associated with
a set of senses $Z_w = \{z_w^1 , z_w^2 , ..., 
z_w^|Z_w|\}$ where
$|Z_w|$ denotes the number of senses discovered for
word w. Each sense $z$ is associated with a distinct
sense-specific embedding $e^z_w$. The first time a 
word $w$ is seen in the text, they maximize the 
probability of seeing the
current token given the context using the global 
embedding $e_w$.
\[
p(e_w|e_{neigh}) = F(e_w,e_{neigh})
\]
where $F$ depends on the type of model. For 
skip-gram, $F(e_w,e_{neigh}) = \prod_{w^{'} \in w_{neigh}} p(e_w, e_w^{'})$ and for CBOW, 
$F(e_w,e_{neigh}) = p(e_w, g(e_{neigh}))$ where 
$g(e_{neigh})$ denotes bag-of-word averaging. 
$e_{neigh}$ includes sense information along with 
the global vectors. 

Then, CRP is used to decide which sense the current
occurance of the word is similar to or create a new 
sense if this meaning was not encountered before. 
The probability for sense association is given by:

\[
Pr(z_w = z) \propto \begin{cases}
N_z^w P(e_w^z|\text{context}) & \text{if $z$ already exists} \\
\gamma P(w|z_{new}) & \text{if $z$ is new}
\end{cases}
\]
where $N_z^w$ denotes the number of times word $w$ has been assigned to sense $z$ and $P(e_w^z|context)$ denotes the
probability of the current word $w$ having sense $z$.

The training approach used in the paper is skip-gram.

\subsubsection{Experiment}
Instead of searching over the space of all senses 
for all words in the document, they use a simplified 
heuristic described below:
\begin{description}
    \item[Greedy Seach] Assign each token locally 
    optimum sense label and use that embedding.
    \item[Expecatation] Represent the word with the 
    expectation over the probability of the sense 
    vectors
    \[
    \overrightarrow{e_z^w} = \sum_{z\in Z_w} 
    p(w|z, \text{context}) e_z^w
    \]
\end{description}


\subsection{Bayesian Skip-gram (BSG)}
This is a method for encoding words as probability 
distributions. The difference with Gaussian mixture 
models is that it is possible to obtain context-specific densities using the Bayesian rule in
this model. The method employs the variational 
autoencoding framework to estimate the posterior 
distribution based on a global prior density.

\subsubsection{Model Details}
This model modifies the skip-gram model by projecting the word vectors to a latent space and 
then predicting the context words based on this 
representation.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{figs/bsg.png}
    \caption{Comparison of Skip-gram and Bayesian skip-gram}
\end{figure}

The model maximizes the variational lower bound of 
marginal likelihood:
\[
\log p_\theta (c|w) \ge \sum_{j=1}^{C} \mathbb{E}_{q_\phi(z|c,w)} [ \log p_\theta (c|z)
 - \mathbb{D}_{KL} [q_\phi(z|c,w) || p_\theta (z|w)]
\]
where $q_\phi(z|c,w)$ is the approximate posterior 
distribution, which can be used to infer the 
representation of the central word $w$ in the context
$c$.

\subsubsection{Experiment}
We can use the inference network $q_\phi(z|c,w)$ to 
estimate the posterior distribution $p_\theta (c|z)$.
A one-layer fully connected layer is used to compute 
the mean and variance parameters of $q$.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{figs/bsg-enc.png}
    \caption{Encoder to approximate posterior 
    distribution}
\end{figure}

In the figure, $R_w$ and $R_{c_j}$ are embeddings of 
the word $w$ and the context words $c_j$.

\clearpage
\section{Tasks}
Since the motivation for developing multi-sense embedding models is to 
account for polysemy and context dependent meaning of words we are using the 
Stanford Contextual Word Similarity (SCWS \citep{huang_improving_2012}) dataset 
to evaluate their ability to represent the meaning of words based on contexts.
We are using a similar contextual word dataset where the same word in used in 
two different contexts (Word in Context dataset \citep{pilehvar_wic_2018})
We are using these embeddings in the following downstream tasks by replacing 
traditional word2vec or Glove embeddings: Named Entity Recognition (NER), 
Sentiment Analysis, Question Answering (QA), Entailment and Semantic Relatedness.

We keep the embeddings fixed during training of the 
deep neural networks for these tasks. We modify the 
implementations provided in the allennlp 
\citep{gardner_allennlp:_2018} library for 
NER, sentiment analysis, QA and entailment tasks. 
Specifically, we wrote scripts to extract embeddings
from the multi-sense embedding models based on the 
context of each batch during training. 

\subsection{Named Entity Recognition}
The NER task uses CoNLL 2003 dataset that was 
made of newswire from the Reuters RCV1 corpus tagged
with four different entity types (\texttt{PER, LOC, 
ORG, MISC}). The baseline model uses pre-trained 
word embeddings, a character-based CNN 
representation, two biLSTM layers and a conditional 
random field (CRF) loss following 
\cite{lample_neural_2016}. 

\subsection{Sentiment Analysis}
The Stanford Sentiment Treebank (SST-5) is used for 
fine-grained sentiment classification where the task 
is to predict one of five labels (from very negative 
to very positive). The baseline model is
the biattentive classification network (BCN) from 
\cite{mccann_learned_2017}.

\subsection{Question Answering}
The Stanford Question
Answering Dataset (SQuAD) contains 100K+ crowd 
sourced questionanswer pairs where the answer is a 
span in a given Wikipedia paragraph. The baseline 
model is an improved version 
\citep{clark_simple_2018} of the
Bidirectional Attention Flow model in 
\cite{seo_bidirectional_2016}. It adds a 
self-attention layer after the bidirectional 
attention component, simplifies some of the pooling 
operations and substitutes the LSTMs for gated 
recurrent units.

\subsection{Entailment}
Textual entailment is the
task of determining whether a hypothesis is
true, given a premise. The Stanford Natural Language 
Inference (SNLI) corpus provides approximately 550K 
hypothesis/premise pairs. Our baseline, the ESIM 
sequence model from \cite{chen_enhanced_2017}, uses 
a biLSTM to encode the premise and hypothesis, 
followed by a matrix attention layer, a local 
inference layer, another biLSTM inference 
composition layer, and finally a pooling operation 
before the output layer.

\subsection{Semantic Relatedness}
We will use the
Sentences Involving Compositional Knowledge
(SICK) dataset (Marelli et al., 2014) consisting of 
9927 sentence pairs. Each
sentence pair is associated with a gold-standard 
label ranging from 1 to 5, indicating how 
semantically related are the two sentences, from 1 
(the two sentences are unrelated) to 5 (the two are 
very related). We can use a biLSTM to encode the sentences into 
vectors and then calculate the similarity score 
following \citep{tai_improved_2015}.

\subsection{SCWS}
Stanford Contextual Word Similarity dataset contains 
two different words in different contexts. It 
contains 2003 word pairs with their example 
sentences. The similarity between the words are 
provided on a scale of 0-10. This dataset poses the 
challenge of finding the appropriate meaning of a 
word based on context. We used cosine similarity to 
predict the score for two words. For the multi-sense
embedding models, we followed the methods described 
before for the embedding models to select the best 
embedding for a given the context. The result is 
given below.

\begin{table}[h]
    \centering
    \begin{tabular}{|c|c|} \hline
        Model & $\rho$ \\ \hline
        SG-100	& 61.49 \\ \hline
        CBOW-100 & \textbf{62.70} \\ \hline
        W2GM-100 (average) &	59.95\\ \hline
        W2GM-100 (best)	& 50.69 \\ \hline
        W2GM-100 (first) &	44.99 \\ \hline
        W2GM-100 (second) &	46.53 \\ \hline
        PFTGM-100 (average) &	35.09 \\ \hline
        PFTGM-100 (best) &	53.59 \\ \hline
        PFTGM-100 (first) &	57.21 \\ \hline
        PFTGM-100 (second) &	26.29 \\ \hline
        Context2Vec-100 (word) &	60.99 \\ \hline
        Context2Vec-100 (word+context) &	31.61 \\ \hline
    \end{tabular}
    \caption{Spearman correlation score ($\rho$) for
    different word embeddings of 100 dimension}
\end{table}

Spearman correlation between two sets of numbers 
is measured by ranking each set from low to high 
and then finding the correlation between these 
ranked numbers.

From the table, we can see that the single-sense 
embedding models are performing better than multi-sense embedding models without using any 
context. For the Word2GM method, the average of the 
mixture means are better than selecting one mixture 
from the context. Similarly for PFTGM, the "best" 
method is inferior to just the first cluster mean 
which is a global word embedding. For context2vec, 
using concatenating the context vector does not 
help, in fact it makes the performance much worse. 
This suggests that, simply concatenating the context 
vector is not a good way to use the information in 
the context vector.

\paragraph{Best Embedding from Context}
For the Word2GM model, we considered another method 
to choose the best mixture cluster based on the 
context in addition to the "best" method described 
before. Here we choose the cluster that maximizes 
the joint probability of all possible combinations 
of embeddings of the target word and its context 
words. The joint probability 
\begin{align*}
    f(w,\text{context}) = &\ p(w_{t-k}, \dots , w_t, \dots , w_{t+k}) \\
    &= p(w_{t-k}, \dots , w_{t+k} | w_t) \\
    &= p(w_{t-k}| w_t)\dots p(w_{t+k} | w_t) p(w_t) \\
    f(w_t^i,\text{context}_j)&= F(v_{t-k}| v_t)\dots F(v_{t+k} | v_t) F(v_t)
\end{align*}

where $w_i$ is the word embedding (mixture of Gaussians) and $v_t = \mu_{w_t}^i$ is the specific embedding (mean of one mixture) for word $w$. So we 
select the embedding $v_t$ for word $w$ that gives 
the maximum joint probability among all possible 
combinations of embeddings. For a context window of 2
(2 words to the left and 2 words to the right) there 
are $2^4=16$ possible combinations for each cluster 
of the mixture embedding $w_t$. So we find the best 
cluster from
\[
\arg \max_{i=1}^2 \{ \max_{j=1}^{2^{2K}} 
f(w_t^i,\text{context}_j) \}
\]
where $K$ is the context window size.

For the Word2GM model, $F(v_1,v_2)$ is given by the  energy function $E_\theta(v_1, v_2)$.

\begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|} \hline 
        Model & Window size & $\rho$ \\ \hline
        W2GM-100 (best2) & 2 &	54.46 \\ \hline
        W2GM-100 (best2) & 3 &	54.62 \\ \hline
        W2GM-100 (best2) & 4 &	\textbf{55.38} \\ \hline
        W2GM-100 (best2) & 5 &	54.82 \\ \hline
        W2GM-100 (best2) & 6 &	54.58 \\ \hline
    \end{tabular}
\end{table}

We can see that this method is better than the 
previous "best" method by around $\approx 4.5\%$ but 
the average method is still better.

\paragraph{Effect of Training Duration}
We investigated the effect of training the Word2GM model for
longer than 3 epochs by measuring performance of the trained 
embeddings on the SCWS data. We can see from Figure \ref{fig:epoch}
that the training the mixture embeddings for longer than 3 epochs
does not offer a significant boost in performance.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\linewidth]{figs/scws-w2g-epoch}
    \caption{Effect of training the Word2GM embeddings for more
    than 3 epochs}
    \label{fig:epoch}
\end{figure}

\subsection{Word in Context (WiC)}
The dataset contains 7612 example word pairs where 
two contexts have the same word. The task is to 
predict if this word has the same meaning in these 
two contexts. The same word is used in different 
forms in these context pairs. So we can measure 
the cosine similarity of the embeddings for these 
two forms ($w$ and $w'$) from the single-sense 
embedding model. If the two words are used in same 
form in these two contexts, then the single-sense 
embedding model will not be able to differentiate 
between them. 
\[
score = CosSim(w,w')
\]

For the multi-sense embedding model (Word2GM), we 
can select the appropriate mixture ($v$ and $v'$) 
based on the context using any of the previous 
methods and then measure the cosine similarity. 
\[
score = CosSim(v,v')
\]

\begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|} \hline 
        Model & Window size & Accuracy (\%) \\ \hline
        SG-100 & NA & 58.05  \\ \hline
        CBOW-100 & NA & 58.20 \\ \hline
        W2GM-100 (average) & NA &	58.13 \\ \hline
        W2GM-100 (best) & 2 &	57.89 \\ \hline
        W2GM-100 (best) & 3 &	58.1 \\ \hline
        W2GM-100 (best) & 4 &	58.47 \\ \hline
        W2GM-100 (best2)& 2 &	58.02 \\ \hline
        W2GM-100 (best2) & 3 &	58.37 \\ \hline
        W2GM-100 (best2) & 4 &	\textbf{58.53} \\ \hline
    \end{tabular}
\end{table}

To predict if the two words are similar based on 
cosine similarity, we need to find a threshold. We 
plot the ROC curve by measuring TPR (True Positive 
Rate) and FPR (False Positive Rate) for different 
threshold values. Then from the ROC curve, we find 
the point where $TPR - FPR$ is maximum. We use this 
threshold for final prediction. The table below 
summarizes the results for different models.

From the table, we can see that multi-sense 
embeddings provide 0.3\% improvement in performance 
than the best single-sense model.

\newpage
\begin{landscape}
\section{Result}
Looking at the performance of the Gaussian mixture model (Word2GM), 
we can conclude that the method of selecting best of two mixtures 
is not optimal since it is performing worse than the simple average 
of the two mixture means in every task except sentiment analysis.
For the Probabilistic FastText embeddings (PFTGM), "average" is not 
a good method as evidenced by their performance in contextual word 
similarity. As we saw in the result of SCWS, the first (global) 
embedding does much better than the second (subword) embedding in 
word similarity. So their average brings the performance down. 
In most of these tasks, average performed the same as the best.
Also the single sense embeddings performed better than both of 
the Gaussian mixture models in all downstream tasks and the simple 
word similarity task. So, they contain more information than any of 
the multi-sense embedding methods without using the context. The 
reason behind this might be that the number of parameters for 
multi-sense embedding models are higher than single-sense model. 
So, despite using the same skip-gram objective these models were 
able to learn better parameters than skip-gram or CBOW. It is also 
possible that the hyperparameters for the training of the multi-sense
embedding models were not optimum compared to the single-sense 
(skip-gram and CBOW) models.

The context2vec model has a training objective similar to CBOW and it 
learns both word and context embedding. Only using the word
embedding, we find that they are performing better than the multi-sense
models. Again, this is not performing as well as CBOW in most cases. 
This indicates that the single-sense models benefit from a more 
efficient implementation and the lower overhead of less parameters to
learn compared to the multi-sense models. All of the downstream tasks
here uses an LSTM to take in the word embeddings and then adds different 
layers on top of the LSTM outputs for final prediction. The ability of 
LSTMs to capture the dependency between words in a context also 
diminishes the advantage of the context specific embeddings of these 
multi-sense models.

\begin{table}[h]
    \centering
    \begin{adjustbox}{width=1.1\linewidth,center=\linewidth}
        \begin{tabular}{c*7{|c}} \hline
            Model $\backslash$ Task & Method & Word Similarity & NER &
            Sentiment Analysis & Relatedness & QA & Entailment \\
            & & SCWS & CoNLL 2003 & SST & SICK & SQUAD
            & SNLI \\
            \hline
            Multi-Sense Embedding
            & (i) Greedy & (i) Greedy & (i) Greedy & (i) Greedy & (i) Greedy & (i) Greedy 
            & (i) Greedy \\
            \citep{li_multi-sense_2015} & (ii) Expectation & (ii) Expectation 
            & (ii) Expectation & (ii) Expectation & (ii) Expectation & (ii) Expectation 
            & (ii) Expectation \\ \hline
%            Dec 25 & & & & & & & \\ 
            Gaussian Mixture Model 
            & (i) Best & (i) 50.69 & (i) 84.85 & (i) 46.6 & (i) Best & (i) 72.93 & (i) 80.26 \\
            \citep{athiwaratkun_multimodal_2017} 
            & (ii) Average & (ii) 59.95 & (ii) 86.27 
            & (ii) 46.52 & (ii) Average & (ii) 74.92 & (ii) 83.33 \\ \hline             
            %            & & & Dec 21 & Dec 21 & Dec 21 & Dec 21 & Dec 21 \\ 
            Probabilistic FastText 
            & (i) Best & (i) 53.59 & (i) 82.89 & (i) 42.08 & (i) Best & (i) 71.6 & (i) 70.19 \\
            \citep{athiwaratkun_probabilistic_2018} 
            & (ii) Average & (ii) 35.09 & (ii) 81.5 
            & (ii) 42.08 & (ii) Average & (ii) 71.6 & (ii) 70.19 \\ \hline
            %            & & & Dec 21 & Dec 21 & Dec 21 & Dec 21 & Dec 21 \\  
            Bayesian Skip-gram 
            & Posterior & Posterior & Posterior & Posterior & Posterior & Posterior & Posterior \\
            \citep{brazinskas_embedding_2018} & & & & & & & \\ \hline
            %            & & Dec 21 & & & & & \\ 
            Context2Vec & (i) Word & (i) 60.99 & (i) 86.91 & (i) 47.01 & (i) Word & (i) 76.53 
            & (i) 82.67 \\
            \citep{melamud_context2vec_2016} & (ii) concat(w,c) & (ii) 31.61 & (ii) concat(w,c) 
            & (ii) concat(w,c) & (ii) concat(w,c) 
            & (ii) concat(w,c) & (ii) concat(w,c) \\ \hline
            %            & & Dec 21 & & & & & \\ 
            Skip-gram & Word & 61.49 & 88.96 & 48.82 & & 76.5 & 84.29 \\ 
            \citep{mikolov_distributed_2013}  & & & & & & & \\ \hline
            %            & & & Dec 21 & Dec 21 & Dec 21 & Dec 21 & Dec 21 \\ 
            CBOW & Word & 62.7 & 89.04 & 48.82 & & 76.2 & 84.71 \\ 
            \citep{mikolov_distributed_2013}  & & & & & & & \\ \hline
            %            & & & Dec 21 & Dec 21 & Dec 21 & Dec 21 & Dec 21 \\ 
        \end{tabular}
    \end{adjustbox}
\end{table}
\end{landscape}

\bibliography{research}

\end{document}