\documentclass[11pt]{article}

\usepackage[margin=1in]{geometry}

\usepackage{graphicx}
\usepackage{natbib}
\bibliographystyle{plainnat}

\begin{document}

\section{Dataset}
I am running experiments on the word-in-context dataset 
\citep{pilehvar_wic_2018}.
The task is binary classification. Each instance in WiC has a target word $w$, 
either a verb or a noun, for which two contexts, $c1$ and $c2$, are provided. 
Each of these contexts triggers a specific meaning of $w$. The task is to 
identify if the occurrences of $w$ in $c1$ and $c2$ correspond to the same 
meaning or not. Table 1 lists some examples from the dataset.

\begin{center}
\begin{table}[h]
\caption{Examples from the dataset}
\footnotesize
\begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}lll}
    \hline 
    F & There’s a lot of trash on the bed of the river & I keep a glass of water 
    next to my bed when I sleep \\
    F & The pilot managed to land the airplane safely & The enemy landed several
    of our aircraft \\
    F & Justify the margins & The end justifies the means \\
    T & We beat the competition & Agassi beat Becker in the tennis championship 
    \\
    T & Air pollution & Open a window and let in some air \\
    T & The expanded window will give us time to catch
    & You have a two-hour window of clear weather to
    \\
    & the thieves & finish working on the lawn \\
    \hline
\end{tabular*}
\normalsize
\end{table}
\end{center}

The statistics of the different splits of WiC dataset are given below.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\linewidth]{figs/wic-stats}
\end{figure}

The authors have not published the test set. However, they 
published the develpment set without labels and there is 
a codalab competition where we can submit predictions on the development set 
to get an accuracy score. I randomly selected 10\% of the training data as a 
test set for running the experiments.

\section{Gaussian Mixture Model}
We are using the Gaussian mixture model for word embeddings from 
\citep{athiwaratkun_multimodal_2017} where each word is represented by a gaussian mixture 
distribution. For a word $w$, the distribution $f_w$ is given by
\[
f_w(\overrightarrow{x}) = \sum_{i=1}^{K} p_{w,i} \mathcal{N}
[\overrightarrow{x}; \overrightarrow{\mu}_{w,i}, 
\Sigma_{w,i}]
\]

For training the embeddings, they use the max-margin ranking objective
\[
L_\theta (w,c,c') = \max(0, m - \log E_\theta (w,c) + \log E_\theta (w,c'))
\]
where $c$ is a context word and $c'$ is a negative sample (out of context) word 
for $w$.  The similarity of a word and its positive context are pushed higher 
than that of its negative context by the margin $m$. They use subsampling and 
negative sampling from $word2vec$.

The energy function is 
\[
E (f,g) = \int f(x) g(x) dx = \left< f,g \right >_{L_2}
\]
where $\left< \right >_{L_2}$ is the inner product in the Hilbert space $L_2$. 
This energy function has a closed form for Gaussian mixtures. So it is a 
better choice than KL divergence.

\section{\textit{context2vec}}
The experiments in the WiC paper showed that the \textit{context2vec} model 
performed the best among the baselines using the concatenation of the two 
sentence vectors. The context2vec model tries to learn a generic embedding  
for variable-length sentential contexts around target words. To do this, they 
use a neural network architecture, which is based on word2vec’s CBOW 
architecture \citep{mikolov_efficient_2013}, but replaces its context modeling 
of averaged word embeddings in a fixed window, with a much more powerful 
neural model, using bidirectional LSTM. The architecture is shown in Figure
\ref{c2v}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\linewidth]{figs/c2v-arch}
    \caption{\textit{context2vec}}
    \label{c2v}
\end{figure}

In their experiments, they use 300 dimensions for the context words and then 
project the output of the LSTM to 600 for getting the sentential context. 
The target word is 600 dimensional. The dimensions of different layers are 
shown in the table.

\begin{table}[h]
    \centering
    \small
\begin{tabular}{|c|c|}
    \hline
    context word units & 300 \\
    LSTM hidden/output units & 600 \\
    MLP input units & 1200 \\
    MLP hidden units & 1200 \\
    sentential context units & 600 \\
    target word units & 600 \\
    negative samples & 10 \\
    \hline
\end{tabular}
\normalsize
\end{table}

They trained their embeddings on the ukWac dataset containing $\sim$2 billion 
words. To speed-up the training of context2vec, they discarded all sentences 
that are longer than 64 words, reducing the size of the corpus by $\sim$10\%.

\section{Embedding Evaluation}
I concatenated the mean of the target word's Gaussian distribution in two 
sentences from the Gaussian mixture model. For the context2vec model, I  
concatenated the context vectors for the two sentences. I compared these  
models with linear classifiers (Logistic Regression and Linear SVM) and a SVM  
with RBF kernel to find the effect of nonlinear features. I selected the  
regularization strength for these models using 5-fold cross validation.

\section{Result}
The nonlinear SVM provides a $\sim$2\% increase in accuracy for the Gaussian  
mixture model on the test set (10\% of training data selected randomly). For  
the context2vec model, there is a $\sim$7\% increase in accuracy for the  
nonlinear SVM compared to linear models. As a result, the context2vec model is 
better than probabilistic model by $\sim$2\% for nonlinear SVM worse by  
$\sim$2\% for the linear classifiers. The 300-dimensional context2vec is 
better than 600-dimension for the nonlinear SVM but worse for the linear  
classifiers.



\bibliography{research}

\end{document}